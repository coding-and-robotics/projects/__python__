# \_\_Python\_\_
## A Python 3 tutorial
  
### Guide
The .PDF "\_\_Python\_\_ - A complete Tutorial" is a guide designed fo solo  
learning and accompanies the example code files. This contains more in depth  
explanations and also advice on how to procede.  
  
 ### Code Samples
Code samples are grouped into parts that loosely corispond to the chapters in  
the guide. Each part is further ordered by prefixing with an alphabetical  
character indicating the logical order in which they have been designed to be  
viewed.  
  
### Presentation
The PowerPoint "\_\_Python\_\_ - A Series of Presentations" is a presentation  
designed to be delivered to a group and covers the fundementals of the code  
examples. This should be used alongside the guide.