try:# Must have only one of these
    pass
except:# Must have at least one of these
    pass
finally:# This is optional
    pass



inputValue = None# Declared here to ensure it stays in scope for the final print statement
while True:# Use of an infinate loop as it will keep going untill the user enters somthing valid
    inputValue = input("Please enter an integer number between 1 and 10 (inclusive):\n>>> ")
    
    try:
        inputValue = int(inputValue)

        if inputValue > 0 and inputValue <= 10:
            break

    except ValueError as e:
        print("Invalid Value. You entered a non-numeric value. Please try again.")

    finally:
        print("The finaly statement was run.")# Try running the program and look where this appears. It should appear BEFORE the loop is exited

print("\nYou inputted " + str(inputValue))

print()



testError = NotImplementedError("a")
try:
    """
    Un-comment each of the following in tern and guess what the output will be then run the file.

    Make sure to re-comment out the previous one!
    """

    #raise testError
    #raise ValueError("b")
    #raise TypeError("c")
    #raise IndexError("d")# Read the result for this one carefully

except NotImplementedError as e:
    print(e)
except (ValueError, TypeError) as e:
    print(e)
except:
    raise testError

finally:
    print("e")



class MyCustomError(Exception):
    pass

raise MyCustomError("This error was custom.")