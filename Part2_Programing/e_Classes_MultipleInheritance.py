class Animal(object):
    def walk(self):
        print("The animal walks foreward.")

    def speak(self):
        print("The animal makes a sound.")

class Quadruped(Animal):
    def __init__(self):
        super().__init__()
        self.legs = 4

    def walk(self):
        print("The animal walks forewards with " + str(self.legs) + " legs.")

class Biped(Animal):
    def __init__(self):
        super().__init__()
        self.legs = 2

    def walk(self):
        print("The animal walks forewards with " + str(self.legs) + " legs.")

class Pet(object):
    def __init__(self, name, owner):
        self.name = name
        self.owner = owner

    def callPet(self):
        print(self.owner.playerName + " says \"Come here " + self.name + "\"")

class Player(object):
    def __init__(self, name):
        self.playerName = name

class Cat(Pet, Quadruped):
    def __init__(self, name, owner):
        super(Cat, self).__init__(name, owner)
        super(Pet, Cat).__init__(self)

    def speak(self):
        print("\"Meow\"")

class Dog(Pet, Quadruped):
    def __init__(self, name, owner):
        super(Dog, self).__init__(name, owner)
        super(Pet, Dog).__init__(self)

    def speak(self):
        print("\"Woof\"")

class Human(Player, Biped):
    def __init__(self, name):
        super(Human, self).__init__(name)
        super(Player, Human).__init__(self)

    def speak(self):
        print("\"Hi\"")

human = Human("Chris")
human.speak()
human.walk()
print(human.legs)

print()

cat = Cat("Lucky", human)
cat.callPet()
cat.speak()
cat.walk()
print(cat.legs)

print()

dog = Dog("Rover", human)
dog.callPet()
dog.speak()
dog.walk()
print(dog.legs)