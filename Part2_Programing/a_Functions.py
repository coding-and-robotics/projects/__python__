# Defines a function
def simplestFunction():
    pass

# Calls a function
simplestFunction()


def functionWithParams(x, y, z):
    print(x, y, z)# <- this is a function too remember!

functionWithParams(0, 1, 2)


def defultArgs(x = 1, y = 2, z = 3):
    print(x, y, z)

defultArgs()

defultArgs(x = 0)
defultArgs(y = 0)
defultArgs(z = 0)

defultArgs(z = 0, y = 0)
defultArgs(z = 0, x = 0)
defultArgs(y = 0, x = 0)

# All of these will do the same thing
defultArgs(0, 0, 0)
defultArgs(0, 0, z = 0)
defultArgs(0, y = 0, z = 0)
defultArgs(x = 0, y = 0, z = 0)

# The order dosen't matter for arguments specified with an =
defultArgs(x = 0, y = 0, z = 0)
defultArgs(y = 0, z = 0, x = 0)
defultArgs(z = 0, x = 0, y = 0)



def returns_a_value(a, b):
    y = a**b
    return y

result = returns_a_value(3, 2)


def returns_a_tuple(a, b):
    y = a // b
    return y, a % b

# Multiple return values require an extra stage
aTuple = returns_a_tuple(3, 2)
result1 = aTuple[0]
result2 = aTuple[1]

# OR

result1, result2 = returns_a_tuple(3, 2)# This is called 'unpacking' - each element of the tuple is unpacked into a variable provided

# The _ can be used in the place of any values that you don't wish to store
result1, _ = returns_a_tuple(3, 2)# Just stores the first value
_, result2 = returns_a_tuple(3, 2)# Just stores the second value
_, _ = returns_a_tuple(3, 2)# Stores nither value but this is pointless as:
returns_a_tuple(3, 2)# will do the same thing!



# Scope example
def globalTestFunction():
    global x
    x = 2

#print(x)# This will fail - un-comment it to see the result
globalTestFunction()
print(x)



# Example of a docstring
def randomInRange(lower, upper, seed = None):
    """
    Selects a random integer from within a defined range of integers using a
    randomly selected seed if one is not provided. The lower bound is
    inclusive and the upper is exclusive so is not a possible result.

    The bounds MUST be different and upper must be larger than the lower.

    Parameters:
        int lower -> The lower bound (inclusive)
        int upper -> The upper bound (exclusive)
        int seed -> The seed to be used to generate the random value.
                    Default is None and results in the use of the current datetime.

    Returns:
        tuple:
            int -> Random integer in the specified range
            int -> The seed used for the random generator
    """

    result = None
    # <code goes here>
    
    return result, seed