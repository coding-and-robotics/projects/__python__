with open("test.txt", "w") as testFile:
    testFile.truncate()
    testFile.writelines(["abc\n", "def\n", "ghi\n"])
    testFile.seek(4, 0)# the string will be written from position 4 onwards in the stream
    testFile.write("n")

    #testFile.write("XYZ")
    #testFile.seek(0, 0)
    #testFile.write("Y")
    #testFile.write("Y")

print("--|| end ||--")

with open("test.txt", "r") as testFile:
    print(testFile.read())