class MyClass(object):
    """
    Document the class here
    """

    def __init__(self):
        self.myAtribute = 0
        self._protectedAtt = "I'm protected."
        self.__privateAtt = 1

    def addANumber(self, number):
        self.myAtribute += number

    def getPrivateAtt(self):
        return self.__privateAtt

    def setPrivateAtt(self, value):
        self.__privateAtt = value

myClass = MyClass()# Instantiates (creates) an objecte from the class
print(myClass.myAtribute)

myClass.myAtribute = 1
print(myClass.myAtribute)

myClass.addANumber(2)
print(myClass.myAtribute)

MyClass.addANumber(myClass, 1)# Alternitive way to access the method - not usualy recomended


print(myClass._protectedAtt)# This works but should be avoided


print(myClass.getPrivateAtt())
myClass.setPrivateAtt("Hi")
print(myClass.getPrivateAtt())

#print(myClass.__privateAtt)# This will cause an error!
print(myClass._MyClass__privateAtt)# Whilst this works it should never be done - don't try to access private attributes like this!



class MagicVowels(object):
    def __init__(self):
        self.__letters = ("a", "e", "i", "o", "u")

    def getVowels(self):
        """
        Defines a getter but no setter as the user shouldn't be able to change the private variable.
        """
        return self.__letters

    def isVowel(self, item):
        """
        A method interface for the __contains__ magic method.
        """
        return self.__contains__(item)

    def __str__(self):
        """
        Allows the object to be nicely converted to a string.
        """
        return "a, e, i, o, u"

    def __len__(self):
        """
        Provides the length of the internal collection.
        """
        return len(self.__letters)

    def __contains__(self, item):
        """
        Tests to see if the test item is a vowel (i.e. is it inside the collection).
        """
        return (item in self.__letters)

    def __itter__(self):
        """
        Returns an itterable from the collection of vowels to allow itteration.
        """
        return self.__letters.__iter__()

    def __eq__(self, item):
        """
        Tests to see if an object is a collection with only the vowels in the correct order.

        Any error caused results in False - this will be covered in a later section.
        """
        try:
            if len(item) != len(self.__letters):
                return False

            passes = True
            for i in range(len(item)):
                passes = item[i] == self.__letters[i]
                if not passes:
                    return False

            return True

        except:
            return False



class IntegerExtention(object):
    def __init__(self, value):
        self.__value = int(value)

    def __str__(self):
        return str(self.__value)

    def getValue(self):
        return self.__value

    def setValue(self, value):
        self.__value = int(value)

    def __eq__(self, testValue):
        if isinstance(testValue, IntegerExtention):
            return int.__eq__(self.__value, testValue.getValue())
        else:
            return int.__eq__(self.__value, testValue)

    def __add__(self, value):
        """
        Adds the values and just returns the result.
        """
        return self.__value + value

    def __iadd__(self, value):
        """
        Updates the private variable with the result of the addition and then returns the object as this now contains the result.
        """
        self.__value += int(value)
        return self# For the __i...__ methods you need to return self. Try commenting out this line and watch what happens.

    def __sub__(self, value):
        return self.__value - value

    def __isub__(self, value):
        self.__value -= int(value)
        return self

    def __mul__(self, value):
        return self.__value * value

    def __imul__(self, value):
        self.__value *= value
        self.__value = int(self.__value)# Done after calculation as otherwise it may produce an unexpected result e.g. 6 * 0.5 = 3 not 6 * int(0.5) = 0
        return self

    def __truediv__(self, value):
        return self.__value / value

    def __itruediv__(self, value):
        self.__value /= value
        self.__value = int(self.__value)# Done after calculation as otherwise it may produce an unexpected result e.g. 6 / 0.5 = 12 not 6 / int(0.5) = divide by zero error
        return self

print(IntegerExtention(1))
print(IntegerExtention(17.4))
print(IntegerExtention(10.8))

print(IntegerExtention(1) == 1.5)# Not equal
print(IntegerExtention(1) == IntegerExtention(1.5))# Equal as the argument gets converted in the constructor

print(IntegerExtention(2) + 5)
#print(5 + IntegerExtention(2))# Fails as no __radd__ method was defined - this is needed for the oposite way arround
testVariable = IntegerExtention(2)
testVariable += 5
print(testVariable)

print(IntegerExtention(2) - 5)
testVariable = IntegerExtention(2)
testVariable -= 5
print(testVariable)

print(IntegerExtention(2) * 0.5)
testVariable = IntegerExtention(2)
testVariable *= 0.5
print(testVariable)

print(IntegerExtention(2) / 0.5)
testVariable = IntegerExtention(2)
testVariable /= 0.5
print(testVariable)



class StaticClass(object):
    staticAtribute = "Parent"

    @staticmethod
    def printAtt():
        print(StaticClass.staticAtribute)

    @classmethod
    def printClassAtt(cls):
        print(cls.staticAtribute)

class StaticOveride(StaticClass):
    staticAtribute = "Child"

# These both print "Parent"
StaticClass.printAtt()
StaticClass.printClassAtt()

StaticClass.printAtt()# This still prints "Parent" as it still uses the parent class's attribute
StaticOveride.printClassAtt()# This prints "Child" as it uses the overloaded atribute from the child class