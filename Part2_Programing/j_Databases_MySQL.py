import mysql.connector# use pip to install "mysql-connector"

# Connects to the database - this type of db (short for DataBase) is
# normaly on a server either localy or hosted
db = mysql.connector.connect(
    host = "localhost",
    user = "username",
    passwd = "password",
    database = "databaseName"
)

cursor = db.cursor()# A cursor is used to work with the database

# executes an SQL query with paramiters
cursor.execute(
    "INSERT INTO tableName (field1, field2) VALUES (%s, %s)",
    ("value1", "value2")
)

# executes the same SQL query on multiple sets of data
cursor.executemany(
    "INSERT INTO tableName (field1, field2) VALUES (%s, %s)",
    [("value3", "value4"), ("value5", "value6"), ("value7", "value8")]
)

db.commit()# This needs to be done after a change is made - think of it like saving a file

# You can check the number of rows involved in the last query
print("Number of rows inserted = {}".format(cursor.rowcount))


# Selects data
cursor.execute("SELECT field1, field2 FROM tableName")# Fetches data from the database

# Retrieves the first item selected by the latest query
singleResult = cursor.fetchone()

# Retrieves all items selected by the latest query
results = cursor.fetchall()

db.close()