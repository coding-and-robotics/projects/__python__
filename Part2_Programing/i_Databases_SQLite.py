import sqlite3

db = sqlite3.connect("databaseName.db")

cursor = db.cursor()

cursor.execute("")

cursor.executemany()

# Retrieves the first item selected by the latest query
cursor.fetchone()

cursor.fetchmany()

# Retrieves all items selected by the latest query
cursor.fetchall()

db.commit()

cursor.close()

db.close()