def fibonacci(numberOfValues, start = (0, 1)):# Switches the order of the paramiters
    counter = 0
    a = start[0]
    b = start[1]# Gives b an initial value

    while counter < numberOfValues:# Adds the ":"
        print(a)# Prints the value of a before it is changed not after

        temp = a# Removes the "1" in front of the variable name
        a = b
        b = temp + a# Removes the "1" in front of the variable name

        counter += 1# Increments the counter to prevent the infinate loop

# Prints the fibonacci sequence for 100 terms
fibonacci(100)