def simplestFunction():
    pass

simplestFunction()


def functionWithParams(x, y, z):
    print(x, y, z)

functionWithParams(0, 1, 2)


def defultArgs(x = 1, y = 2, z = 3):
    print(x, y, z)

defultArgs()

defultArgs(x = 0)
defultArgs(y = 0)
defultArgs(z = 0)

defultArgs(z = 0, y = 0)
defultArgs(z = 0, x = 0)
defultArgs(y = 0, x = 0)

defultArgs(0, 0, 0)
defultArgs(0, 0, z = 0)
defultArgs(0, y = 0, z = 0)
defultArgs(x = 0, y = 0, z = 0)

defultArgs(x = 0, y = 0, z = 0)
defultArgs(y = 0, z = 0, x = 0)
defultArgs(z = 0, x = 0, y = 0)



def returns_a_value(a, b):
    y = a**b
    return y

result = returns_a_value(3, 2)


def returns_a_tuple(a, b):
    y = a // b
    return y, a % b

aTuple = returns_a_tuple(3, 2)
result1 = aTuple[0]
result2 = aTuple[1]

result1, result2 = returns_a_tuple(3, 2)

result1, _ = returns_a_tuple(3, 2)
_, result2 = returns_a_tuple(3, 2)
_, _ = returns_a_tuple(3, 2)
returns_a_tuple(3, 2)



def globalTestFunction():
    global x
    x = 2

globalTestFunction()
print(x)



def randomInRange(lower, upper, seed = None):
    """
    Selects a random integer from within a defined range of integers using a
    randomly selected seed if one is not provided. The lower bound is
    inclusive and the upper is exclusive so is not a possible result.

    The bounds MUST be different and upper must be larger than the lower.

    Parameters:
        int lower -> The lower bound (inclusive)
        int upper -> The upper bound (exclusive)
        int seed -> The seed to be used to generate the random value.
                    Default is None and results in the use of the current datetime.

    Returns:
        tuple:
            int -> Random integer in the specified range
            int -> The seed used for the random generator
    """

    result = None
    # <code goes here>
    
    return result, seed