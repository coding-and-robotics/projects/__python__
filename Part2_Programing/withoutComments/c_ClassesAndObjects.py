class MyClass(object):
    """
    Document the class here
    """

    def __init__(self):
        self.myAtribute = 0
        self._protectedAtt = "I'm protected."
        self.__privateAtt = 1

    def addANumber(self, number):
        self.myAtribute += number

    def getPrivateAtt(self):
        return self.__privateAtt

    def setPrivateAtt(self, value):
        self.__privateAtt = value

myClass = MyClass()
print(myClass.myAtribute)

myClass.myAtribute = 1
print(myClass.myAtribute)

myClass.addANumber(2)
print(myClass.myAtribute)

MyClass.addANumber(myClass, 1)


print(myClass._protectedAtt)


print(myClass.getPrivateAtt())
myClass.setPrivateAtt("Hi")
print(myClass.getPrivateAtt())

#print(myClass.__privateAtt)
print(myClass._MyClass__privateAtt)



class MagicVowels(object):
    def __init__(self):
        self.__letters = ("a", "e", "i", "o", "u")

    def getVowels(self):
        return self.__letters

    def isVowel(self, item):
        return self.__contains__(item)

    def __str__(self):
        return "a, e, i, o, u"

    def __len__(self):
        return len(self.__letters)

    def __contains__(self, item):
        return (item in self.__letters)

    def __itter__(self):
        return self.__letters.__iter__()

    def __eq__(self, item):
        try:
            if len(item) != len(self.__letters):
                return False

            passes = True
            for i in range(len(item)):
                passes = item[i] == self.__letters[i]
                if not passes:
                    return False

            return True

        except:
            return False



class IntegerExtention(object):
    def __init__(self, value):
        self.__value = int(value)

    def __str__(self):
        return str(self.__value)

    def getValue(self):
        return self.__value

    def setValue(self, value):
        self.__value = int(value)

    def __eq__(self, testValue):
        if isinstance(testValue, IntegerExtention):
            return int.__eq__(self.__value, testValue.getValue())
        else:
            return int.__eq__(self.__value, testValue)

    def __add__(self, value):
        return self.__value + value

    def __iadd__(self, value):
        self.__value += int(value)
        return self

    def __sub__(self, value):
        return self.__value - value

    def __isub__(self, value):
        self.__value -= int(value)
        return self

    def __mul__(self, value):
        return self.__value * value

    def __imul__(self, value):
        self.__value *= value
        self.__value = int(self.__value)
        return self

    def __truediv__(self, value):
        return self.__value / value

    def __itruediv__(self, value):
        self.__value /= value
        self.__value = int(self.__value)
        return self

print(IntegerExtention(1))
print(IntegerExtention(17.4))
print(IntegerExtention(10.8))

print(IntegerExtention(1) == 1.5)
print(IntegerExtention(1) == IntegerExtention(1.5))

print(IntegerExtention(2) + 5)
#print(5 + IntegerExtention(2))
testVariable = IntegerExtention(2)
testVariable += 5
print(testVariable)

print(IntegerExtention(2) - 5)
testVariable = IntegerExtention(2)
testVariable -= 5
print(testVariable)

print(IntegerExtention(2) * 0.5)
testVariable = IntegerExtention(2)
testVariable *= 0.5
print(testVariable)

print(IntegerExtention(2) / 0.5)
testVariable = IntegerExtention(2)
testVariable /= 0.5
print(testVariable)



class StaticClass(object):
    staticAtribute = "Parent"

    @staticmethod
    def printAtt():
        print(StaticClass.staticAtribute)

    @classmethod
    def printClassAtt(cls):
        print(cls.staticAtribute)

class StaticOveride(StaticClass):
    staticAtribute = "Child"

StaticClass.printAtt()
StaticClass.printClassAtt()

StaticClass.printAtt()
StaticOveride.printClassAtt()