class Animal(object):
    def walk(self):
        print("The animal walks foreward.")

    def speak(self):
        print("The animal makes a sound.")

class Quadruped(Animal):
    def __init__(self):
        self.legs = 4

    def walk(self):
        print("The animal walks forewards with " + str(self.legs) + " legs.")

class Biped(Animal):
    def __init__(self):
        self.legs = 2

    def walk(self):
        print("The animal walks forewards with " + str(self.legs) + " legs.")

class Cat(Quadruped):
    def __init__(self):
        super().__init__()

    def speak(self):
        print("\"Meow\"")

class Dog(Quadruped):
    def __init__(self):
        super().__init__()

    def speak(self):
        print("\"Woof\"")

class Human(Biped):
    def __init__(self):
        super().__init__()

    def speak(self):
        print("\"Hi\"")

human = Human()
human.speak()
human.walk()
print(human.legs)

print()

cat = Cat()
cat.speak()
cat.walk()
print(cat.legs)

print()

dog = Dog()
dog.speak()
dog.walk()
print(dog.legs)