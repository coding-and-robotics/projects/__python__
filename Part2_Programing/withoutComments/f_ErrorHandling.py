try:
    pass
except:
    pass
finally:
    pass



inputValue = None
while True:
    inputValue = input("Please enter an integer number between 1 and 10 (inclusive):\n>>> ")
    
    try:
        inputValue = int(inputValue)

        if inputValue > 0 and inputValue <= 10:
            break

    except ValueError as e:
        print("Invalid Value. You entered a non-numeric value. Please try again.")

    finally:
        print("The finaly statement was run.")

print("\nYou inputted " + str(inputValue))

print()



testError = NotImplementedError("a")
try:
    #raise testError
    #raise ValueError("b")
    #raise TypeError("c")
    #raise IndexError("d")# Read the result for this one carefully

except NotImplementedError as e:
    print(e)
except (ValueError, TypeError) as e:
    print(e)
except:
    raise testError

finally:
    print("e")



class MyCustomError(Exception):
    pass

raise MyCustomError("This error was custom.")