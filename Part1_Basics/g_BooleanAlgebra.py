# Try changing these and see if you can work out what the outputs should be
a = True
b = False

print(a and b)# a AND b

print(a or b)# a OR b

print(a != b)# This is equivilant to: a XOR b

print(not a)# NOT a

print(a == b)# Equality Check

print(5 > 20)# More than

print(4 >= 4)# More than or equal to

print(26 < 50)# Less than

print(10 <= 200)# Less than or equal to

# The == and != can also work with non Booleans
print("Hi" != "this isn't Hi")

print(30 == 10 + 20)

inputValue = "No"
print("Yes" == inputValue)

# They can also be combined
print(True and (False or True and False))