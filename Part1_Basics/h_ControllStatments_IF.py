# Try altering the variables below and see what happens
testValue1 = "A test string"
testValue2 = ""
testValue3 = 180

"""
IF
"""
# Remember that the == is used to check if things are equal
if testValue1 == "A test string":
    print("A test string was present.")


"""
IF ELSE
"""
if testValue2 != "":# If the string is NOT empty
    print(testValue2)
else:# Does the same thing as "    if testValue2 == "":    "
    print("The string was empty!")


"""
IF ELIF ELSE
"""
if testValue3 <= 90:
    print("An accute angle was provided.")
elif testValue3 <= 180:
    print("An obtuce angle was provided.")
elif testValue3 <= 360:
    print("A reflex angle was provided.")
else:# More than 360 degrees
    print("The angle provided was not real.")