# All data stored by a variable has a type, the most basic of which is an 'object'
# All other 'types' are more complex objects
# Python has several allready built in. The most basic are the following:

1# integer (int) - a number with NO decimal places

1.8# float (float) - a number WITH decimal places

"text here"# string (str) - text. Can also be provided with single quotes (see below)
'text here'# string (str) - text

"a"# character - a string with a single item (not actualy a data type as in other languages)

True# boolean (bool) - can be either 'True' or 'False', great for yes/no and on/off situations
False# boolean (bool)

None# null - absence of data - see guide for more info


# Unlike other languages (C, VB, Java, etc.), Python variables aren't fixed to one data type
var1 = 1# An integer
var1 = "string"# Changed to a string
var1 = True# Now a boolean
# This can be usefull but can also lead to mistakes - 1 + "1" won't equal 2 (more on this later)


# Python also has an easy way to convert between data types
data = 0

dataStr = str(data)# Converts to a string
print(dataStr)

dataInt = int(dataStr)# Converts to an integer
print(dataInt)

dataFloat = float(data)# Converts to a float
print(dataFloat)

dataBool = bool(data)# Converts to a boolean
print(dataBool)