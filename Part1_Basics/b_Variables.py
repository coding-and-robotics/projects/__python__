# A variable can hold data for later use in the program

# The following line is an example of one:
myVariable = 0# This is a variable with a value of 1

# To see what is in a variable, the easyest way is to 'print' it to the console window
print(myVariable)# This will write "1" to the console

# A program can have as many variables as you like
# Provided that a few rules are followed, they can be called almost anything you like too!
myVariable2 = 2
thisIsAVariableToo = "text"
yet_annother_variable = 57.892
sTILLavARIABLE = 4
fduhibjnkbhjfgbhirgeuhi = 5# This is a valid variable but NEVER EVER do this!

# As their name suggests, 'variables' can have 'variable contence
# As a result the data they hold can be changed as follows:
myVariable = 1
print(myVariable)# The console should now have the number 1 written to it

# A variable can also be given a value using annother variable. For example:
myVariable = myVariable2
print(myVariable)# The console should now have the number 2 written to it

# This is the end of the program. All variables will now be deleted