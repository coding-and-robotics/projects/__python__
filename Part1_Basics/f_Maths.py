"""
The ability to do mathematics is arguably one of Python's most important features
In order to allow this, Python code uses a set of 'operators' which are (in most cases)
the same as those used in mathematics.

Each can be used in two ways: normal use or in conjunction with an "=" which uses the
value of the variable used to store the result before the operation is calculated
"""

#Note: these operators can be 'overloaded' however this is an advanced topic - part of OOP

a = 1
b = 2

"""
Basic Operators
"""

# "+" adds two items
result = a + b
result += 1

# "-" subtracts one item from annother
result = a - b
result -= 1

# "*" multyplys two items
result = a * b
result *= 1

# "/" divides one item by another
result = a / b
result /= 1

"""
Advanced Operators
"""

# "%" finds the modulo (remandor of the division) of two items
result = a % b
result %= 1

# "//" (integer division) divides one item by another (if both items are int then the result is truncated to make an int also)
result = a // b
result //= 1

# "**" raises one item to the power of the other
result = a**b
result **= 1

# Note: to square root a number an external package is required. "math" and "numpy" both provide this functionality

"""
Binary Operators

There also exist a set of operators used for performing operations on binary numbers.
If you know what these operations are, great! If not, don't worry too much as you are unlikely to use them
they are just here for completeness.
"""

x = 0b1100# These are binary numbers as they are prefixed with "0b"
y = 0b1010

# "&" binary AND
result = x & y
result &= y

# "|" binary OR
result = x | y
result |= y

# "^" binary XOR
result = x ^ y
result ^= y

# "~" binary NOT
result = ~x
# Note this has no alternitive form - the equivilant is just:
result = ~result

# "<<" binary Left Shift
result = x << 2
result <<= 2

# ">>" binary Right Shift
result = x >> 2
result >>= 2