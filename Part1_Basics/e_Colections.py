# How could many items of data be stored?
dog1 = 1
dog2 = 2
dog3 = 3
dog4 = 4
dog5 = 5
dog6 = 6

# But what would happen if there were 50 dogs? What about 1000? What if you don't know how many are needed?!
# To solve this, Python has a type of data called a 'list'. These hold lots of other peices of data.

myList = []# This creates a blank list

myList2 = [1, 2, 3, 4, 5]# This creates a list with the numbers 1 to 5 in it. All elements are seperated by a comma

# To add an item to the list, do the following:
myList2.append(6)

# Lists can be printed using 'print':
print(myList2)

# To get at the data again, you request the data at an 'index' where the first index is '0'
# This looks like myListVariable[index number in here] e.g.:
print(myList2[0])# This prints the number 1 which is in position 0

# You can also access lists from the other direction, myListVariable[-1] gives the last item, -2 the second to last, etc.
print(myList2[-3])# Prints the number 3 which is in the third to last position

# The value at an index in a list can be changed as follows:
myList2[0] = 100
# Note how this is exactly the same as setting a variable - the variable name is replaced by the reference to the list's data

# A list can contain more than one type of data:
multiDataList = [1, "Text", False, myList2]
# Note how lists can contain other lists too!

# A single list has 1 dimention. To get a 2D list, they can be 'nested'. E.g.:
secondDimentionList = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
# This can be done  for any number of dimentions needed (even 4D and beond however this can get a little confusing)



# Another colection of data exists called a 'tuple'. These are created like follows:
tuple1 = (1, "some text", True)

# These are used like lists
print(tuple1[1])# Prints "some text"

# However they can't be changed once they have been created!
# The lines below will produce an error - try uncommenting them one at a time and running the file
#tuple1.append(5)
#tuple1[0] = 3

# Tuples can also be nested:
tuple2 = ((1, 2), (3, 4))



# Some times, data comes in pairs. A 'dictionary' ('dict' for short and sometimes called an 'associative array') can have ...
# A dict can be created as follows:
dict1 = {"key1": "value1", "key2": "value2", "key3": "value3"}

# In order to make this look a little nicer, the following can be done:
dict1 = {"key1": "value1",
         "key2": "value2",
         "key3": "value3"}# This can be done with any of the collections in this file too!

# The data in a dict is split into 'keys' and 'values' - one key to one value and each key MUST be unique
# Prety much any data type can be a key

# To add a new item, do the following:
dict1["new key name"] = "new value"

# To access the data, the key is used:
print(dict1["key2"])# prints "value2"

# As a result, a dictionary with integers as keys acts like a list! But don't do this - use a list instead.



# A good example is student's test scores:
passingGrades = {"A*": True,
                 "A": True,
                 "B": True,
                 "C": True,
                 "D": False,
                 "E": False,
                 "F": False,
                 "U": False}

student_infomation = [("Chris", "A*"),
                      ("Jake", "F"),
                      ("Lauren", "B")]

# Try to guess the results of the following lines. Run the file to see if you were right.
print()
print("Did " + student_infomation[0][0] + " pass with grade " + student_infomation[0][1] + "? " + str(passingGrades[student_infomation[0][1]]))
print("Did " + student_infomation[1][0] + " pass with grade " + student_infomation[1][1] + "? " + str(passingGrades[student_infomation[1][1]]))
print("Did " + student_infomation[2][0] + " pass with grade " + student_infomation[2][1] + "? " + str(passingGrades[student_infomation[2][1]]))

# Wouldn't this take forever if there were a whole class of students here?
# Try to think how this could be made easier. The answer will be covered in the next part!