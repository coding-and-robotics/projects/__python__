# IO stands for Input and Output. This is how a program interfaces with other things
# This can be a user, a file, other programs, etc.

# The most basic form of IO in python is with the console - a text based interface
# this is the deafult when a .py file is double clicked and run by a user

# Simple output is easy with Python - you have allready seen it!
print("output data")
# This will output the data or variable between the brackets along with a new line
# To output multiple things at once:
print("thing 1", "thing 2", 3, 4, 5, "etc.")
# This will put spaces between the data when it outputs them in order



# Help for running this program - can be ignored
print("If the line is blank, just press enter in this program.")



# Input is a little more complex
input()
# This waits for the user to press the 'Enter' key, anything the user has typed before the 'Enter' is collected

# The result is a string which can be used immediately:
print(input())
# or stored in a variable:
result = input()


# In order to help the user, an optional string can be provided:
input("Input somthing and then press the enter key:")


# As mentioned, 'input()' produces a string
stringVariable = input("Input an integer:")
print("This was stored as a string:", stringVariable)

# In order to get a different type, the result needs to be converted:
intVariable = int(input("Input an integer:"))
print("This was stored as an integer:", intVariable)
# However, what happens if the user provides a string with one ore more non integer characters or a number with a decimal part?
# Run the program and try!

intVariable = int(input("Try to input a string or float instead (hint: it won't work):"))
# Don't be worried by the error message this produces
# All that Python is doing is telling you that somthing was wrong before it stops running the code
# See if you can work out what it's telling you - explained in the guide