# Try to write a simple program that asks the user for two ages seperated by no more than 20 years,
# display the diference and whether or not it is a prime number. Some code and hints have been provided:

# Define usefull data:
primes = [False, True, True, False, True, False, True, False, False, False, True, False, True, False, False, False, True, False, True, False]

# Get two numbers from the user (don't forget to explain to the user what it is you want!):
print("Please input two ages in years seperated by no more than 20 years. The first should be smaller than the second.")
number1 = int(input("The first number: "))
number2 = int(input("The second number: "))
print()

# Use the numbers to determine the difference and determine if it is prime:
difference = number2 - number1
isPrime = primes[difference - 1]

# Display the results:
print("The difference in the ages provided is " + str(difference) + " years.")
print("Is " + str(difference) + " years prime? " + str(isPrime))

input("Press enter to exit...")