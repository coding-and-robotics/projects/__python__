dog1 = 1
dog2 = 2
dog3 = 3
dog4 = 4
dog5 = 5
dog6 = 6


myList = []

myList2 = [1, 2, 3, 4, 5]

myList2.append(6)

print(myList2)

print(myList2[0])

print(myList2[-3])

myList2[0] = 100

multiDataList = [1, "Text", False, myList2]

secondDimentionList = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]



tuple1 = (1, "some text", True)

print(tuple1[1])

tuple2 = ((1, 2), (3, 4))



dict1 = {"key1": "value1", "key2": "value2", "key3": "value3"}

dict1 = {"key1": "value1",
         "key2": "value2",
         "key3": "value3"}

dict1["new key name"] = "new value"

print(dict1["key2"])



passingGrades = {"A*": True,
                 "A": True,
                 "B": True,
                 "C": True,
                 "D": False,
                 "E": False,
                 "F": False,
                 "U": False}

student_infomation = [("Chris", "A*"),
                      ("Jake", "F"),
                      ("Lauren", "B")]

print()
print("Did " + student_infomation[0][0] + " pass with grade " + student_infomation[0][1] + "? " + str(passingGrades[student_infomation[0][1]]))
print("Did " + student_infomation[1][0] + " pass with grade " + student_infomation[1][1] + "? " + str(passingGrades[student_infomation[1][1]]))
print("Did " + student_infomation[2][0] + " pass with grade " + student_infomation[2][1] + "? " + str(passingGrades[student_infomation[2][1]]))