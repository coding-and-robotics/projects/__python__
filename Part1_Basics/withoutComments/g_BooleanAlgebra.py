a = True
b = False

print(a and b)

print(a or b)

print(a != b)# This is equivilant to: a XOR b

print(not a)

print(a == b)

print(5 > 20)

print(4 >= 4)

print(26 < 50)

print(10 <= 200)


print("Hi" != "this isn't Hi")

print(30 == 10 + 20)

inputValue = "No"
print("Yes" == inputValue)


print(True and (False or True and False))