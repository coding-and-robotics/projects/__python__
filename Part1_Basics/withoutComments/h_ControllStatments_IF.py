testValue1 = "A test string"
testValue2 = ""
testValue3 = 180

"""
IF
"""
if testValue1 == "A test string":
    print("A test string was present.")


"""
IF ELSE
"""
if testValue2 != "":
    print(testValue2)
else:
    print("The string was empty!")


"""
IF ELIF ELSE
"""
if testValue3 <= 90:
    print("An accute angle was provided.")
elif testValue3 <= 180:
    print("An obtuce angle was provided.")
elif testValue3 <= 360:
    print("A reflex angle was provided.")
else:
    print("The angle provided was not real.")