a = 1
b = 2

"""
Basic Operators
"""

result = a + b
result += 1

result = a - b
result -= 1

result = a * b
result *= 1

result = a / b
result /= 1

"""
Advanced Operators
"""

result = a % b
result %= 1

result = a // b
result //= 1

result = a**b
result **= 1

# Note: to square root a number an external package is required. "math" and "numpy" both provide this functionality

"""
Binary Operators
"""

x = 0b1100# These are binary numbers as they are prefixed with "0b"
y = 0b1010

result = x & y
result &= y

result = x | y
result |= y

result = x ^ y
result ^= y

result = ~x
# Note this has no alternitive form - the equivilant is just:
result = ~result

result = x << 2
result <<= 2

result = x >> 2
result >>= 2