startNumber = 260

number = startNumber
while float(number // 2) == number / 2 :
    number = number // 2

print("The original number was \"" + str(startNumber) + "\" and the smallest number reached was \"" + str(number) + "\".")



for i in range(0, 10):
    print(str(i) + " squared is " + str(i**2))



primeNumbers = [2, 3, 5, 7]

number = 8
while len(primeNumbers) < 100:
    isPrime = True

    for checkNumber in range(2, 10):
        isPrime = float(number // checkNumber) != number / checkNumber
        if not isPrime:
            break

    if not isPrime:
        number += 1
        continue

    for prime in primeNumbers:
        isPrime = float(number // prime) != number / prime
        if not isPrime:
            break

    if isPrime:
        primeNumbers.append(number)

    number += 1

print(primeNumbers)