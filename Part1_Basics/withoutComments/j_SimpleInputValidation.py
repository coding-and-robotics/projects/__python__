validInputs = ("Yes", "yes", "Y", "y", "No", "no", "N", "n")

userInput = None
while True:
    userInput = input('Please enter "Yes" or "No":\n>>> ')

    if userInput in validInputs:
        break

    else:
        print('That was not a valid input! Please enter either "Yes" or "No".')

print("\nYou entered \"" + userInput + "\"")